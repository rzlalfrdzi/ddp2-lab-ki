import java.util.Scanner;
class Lab8 {

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        Korporasi korp = new Korporasi();
        int perintah = x.nextInt();
        Karyawan.setLimitGajiStaff(perintah);
        while (true) {

            String inp = x.nextLine();
            String[] inpSplited = inp.split(" ");

            switch (inpSplited[0]) {

                case "TAMBAH_KARYAWAN":
                    if (inpSplited.length == 4) {
                        korp.addKaryawan(inpSplited[2], inpSplited[1], Integer.parseInt(inpSplited[3]));
                    }
                    break;

                case "STATUS":
                    if (inpSplited.length == 2) {
                        korp.printStatus(inpSplited[1]);
                    }
                    break;

                case "TAMBAH_BAWAHAN":
                    if (inpSplited.length == 3) {
                        Karyawan atasan = korp.find(inpSplited[1]);
                        Karyawan bawahan = korp.find(inpSplited[2]);
                        if (atasan == null || bawahan == null) {
                            System.out.println("Nama tidak berhasil ditemukan");
                            break;
                        } else if (atasan.getTipe().equalsIgnoreCase("Manager")) {
                            ((Manager) atasan).addBawahan(inpSplited[2]);
                            break;
                        } else if (atasan.getTipe().equalsIgnoreCase("Staff")) {
                            ((Staff) atasan).addBawahan(inpSplited[2]);
                            break;
                        } else {
                            ((Intern) atasan).addBawahan(inpSplited[2]);
                            break;
                        }
                    }

                case "GAJIAN":
                korp.gajian();
                    break;
            }
        }
    }
}

