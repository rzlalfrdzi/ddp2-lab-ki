public class Intern extends Karyawan {
    public Intern(String nama, int gaji) {
        super(nama, gaji);
        this.tipe = "Intern";
    }

    protected void addBawahan(String namaDitambah) {
        System.out.println("Anda tidak layak memiliki bawahan");
    }
}