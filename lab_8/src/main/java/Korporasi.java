import java.util.ArrayList;
import java.util.Objects;

public class Korporasi {
    private ArrayList<Karyawan> namesOfKaryawan = new ArrayList<>();

    protected Karyawan find(String nama) {
        for (Karyawan karya : namesOfKaryawan) {
            if (nama.equalsIgnoreCase(karya.getNama())) return karya;
        }
        return null;
    }

    protected void addKaryawan(String tipe, String nama, int gajiAwal) {
        if (find(nama) != null) {
            System.out.println("Karyawan dengan nama " + nama.toUpperCase() + " telah terdaftar");
            return;
        } else if (namesOfKaryawan.size() == Karyawan.MAKS_KARYAWAN) {
            System.out.println("Karyawan sudah mencukupi kuota maks");
        }
        switch (tipe) {
            case "MANAGER":
                namesOfKaryawan.add(new Manager(nama, gajiAwal));
                break;
            case "INTERN":
                namesOfKaryawan.add(new Intern(nama, gajiAwal));
                break;
            case "STAFF":
                namesOfKaryawan.add(new Staff(nama, gajiAwal));
                break;
        }
        System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
    }

    protected void printStatus(String nama) {
        if (find(nama) == null) {
            System.out.println("Karyawan tidak ditemukan");
            return;
        }
        System.out.println(nama + " " + find(nama).getGaji());
    }


    protected void gajian() {
        System.out.println("Semua karyawan telah diberikan gaji");
        for (int i = 0; i < namesOfKaryawan.size(); i++) {
            Karyawan kary = namesOfKaryawan.get(i);
            int temp1 = kary.getCountGaji();
            kary.setCountGaji(temp1 + 1);
            int temp2 = kary.getCountGaji();
            if (temp2 == 6) {
                naikGaji(kary.getNama());
                kary.setCountGaji(0);
            }
        }
    }

    private void naikGaji(String nama) {
        int gajiLama = Objects.requireNonNull(find(nama)).getGaji();
        int gajiBaru = (int) (gajiLama * 1.1);
        Objects.requireNonNull(find(nama)).setGaji(gajiBaru);
        System.out.println(nama + " mengalami kenaikan gaji sebesar 10% dari " + gajiLama +
                " menjadi " + gajiBaru);
        if (Objects.requireNonNull(find(nama)).getTipe().equals("Staff")) {
            if (Objects.requireNonNull(find(nama)).getGaji() >= Karyawan.getLimitGajiStaff()) {
                Karyawan baru = new Manager(nama, gajiBaru);
                namesOfKaryawan.remove(namesOfKaryawan.indexOf(find(nama)));
                namesOfKaryawan.add(baru);
                if (find(nama).getListBawahan() != null) {
                    for (Karyawan kary : Objects.requireNonNull(find(nama)).getListBawahan()) {
                        baru.getListBawahan().add(kary);
                    }
                }
                System.out.println("Selamat, " + nama.toUpperCase() + " telah dipromosikan menjadi MANAGER");
            }
        }
    }
}

