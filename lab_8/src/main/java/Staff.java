public class Staff extends Karyawan {
    public Staff(String nama, int gaji) {
        super(nama, gaji);
        this.tipe = "Staff";

    }

    private Karyawan find(String nama) {
        for (Karyawan karya : ListBawahan) {
            if (nama.equalsIgnoreCase(karya.getNama())) return karya;
        }
        return null;
    }

    protected void addBawahan(String namaDitambah) {
        if (find(namaDitambah) == null){
            ListBawahan.add(find(namaDitambah));
            System.out.println("Karyawan " + namaDitambah.toUpperCase() + " berhasil ditambahkan menjadi " +
                    "bawahan " + nama.toUpperCase());
            return;
        }
        else{
            if (find(namaDitambah).getTipe() == "Manager" || find(namaDitambah).getTipe() == "Staff") {
                System.out.println("Anda tidak layak memiliki bawahan");
                return;
            } else if (ListBawahan.contains(find(namaDitambah))) {
                System.out.println("Karyawan " + namaDitambah + " telah menjadi bawahan " + nama);
                return;
            } else if (ListBawahan.size() >= 10) {
                System.out.println("Bawahan sudah memenuhi kuota maksimal");
                return;
            }
            ListBawahan.add(find(namaDitambah));
            System.out.println("Karyawan " + namaDitambah.toUpperCase() + " berhasil ditambahkan menjadi " +
                    "bawahan " + nama.toUpperCase());
        }
    }
}
