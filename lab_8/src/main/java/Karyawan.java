import java.util.ArrayList;

public class Karyawan {
    String nama,tipe;
    private int gaji;
    protected static final int MAKS_KARYAWAN = 10000;
    ArrayList<Karyawan> ListBawahan;
    private int countGaji = 0;
    private static int MAX_GAJI_STAFF = 5000;

    Karyawan(String nama, int gaji){
        this.nama = nama;
        this.gaji = gaji;
        this.ListBawahan = new ArrayList<>();
        this.tipe = "";
    }

    public String getNama() {
        return nama;
    }

    public int getGaji() {
        return gaji;
    }

    public String getTipe() {
        return tipe;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getCountGaji() {
        return countGaji;
    }

    public void setCountGaji(int countGaji) {
        this.countGaji = countGaji;
    }

    public static void setLimitGajiStaff(int gaji) {
        MAX_GAJI_STAFF = gaji;
    }
    public static int getLimitGajiStaff(){
        return MAX_GAJI_STAFF;
    }

    public ArrayList<Karyawan> getListBawahan() {
        return ListBawahan;
    }
}
