package pquiz1.reader;
import pquiz1.enrollment.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.nio.charset.StandardCharsets;

public class RecordsReader implements CsvReader {
    Path file;
    List<String> line;
    ArrayList<Student>students;

    public RecordsReader(Path file) throws IOException{
        this.file =file;
        this.listStudents();
        this.line = Files.readAllLines(this.file, StandardCharsets.UTF_8);
        this.line.remove(0);
        this.students = new ArrayList<>();

    }

    protected void listStudents(){
        for(String line :this.line){
            String[] data = line.split(",");

        }
    }

    @Override
    public List<Student> getStudents() {
        return null;
    }

    @Override
    public List<Student> getStudentsEnrolledInCourse(String courseName) {
        return null;
    }

    @Override
    public List<Student> getStudentsByEnrollYear(int year) {
        return null;
    }
}
