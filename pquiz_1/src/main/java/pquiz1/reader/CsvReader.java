package pquiz1.reader;

import pquiz1.enrollment.Student;

import java.util.ArrayList;

import java.util.List;

import java.io.FileReader;

import java.io.IOException;


public interface CsvReader {

    /**
     * Returns list of unique students.
     *
     * @return
     */
    List<Student> getStudents();

    /**
     * Returns list of unique students enrolled in a given course.
     *
     * @param courseName
     * @return
     */
    public List<Student> getStudentsEnrolledInCourse(String courseName);

    /**
     * Returns list of unique students enrolled in a given year.
     * @param year
     * @return
     */
    public List<Student> getStudentsByEnrollYear(int year);
}
