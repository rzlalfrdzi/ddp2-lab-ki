package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * A class representing an event and its properties
 */
public class Event {
    /**
     * Name of event
     */
    private String name;
    private GregorianCalendar startTime, endTime;
    private BigInteger costPerHour;

    public Event(String name, GregorianCalendar startTime, GregorianCalendar endTime, BigInteger costPerHour) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }


    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    public BigInteger getCost() {
        return costPerHour;
    }

    /**TODO
     * @return
     */
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return getName() + "\n" +
                "Waktu mulai: " + dateFormat.format(this.startTime.getTime()) + "\n" +
                "Waktu selesai: " + dateFormat.format(this.endTime.getTime()) + "\n" +
                "Biaya kehadiran: "+ this.costPerHour+"\n";
    }


    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
