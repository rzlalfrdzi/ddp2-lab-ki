package lab9;

import lab9.event.Event;
import lab9.user.User;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    private User getUser(String name) {
        for (User user : users) {
            if (name.equals(user.getName())) return user;
        }
        return null;
    }

    private Event getEvent(String name) {
        for (Event event : events) {
            if (name.equals(event.getName())) return event;
        }
        return null;
    }

    /**
     * fungsi untuk mendaftarkan event
     *
     * @param name, name adalah string yg menyimpan nama event.
     * @param startTimeStr adalah String yg menunjukkan waktu dimulai nya acara
     * @param endTimeStr adalah String yg menunjukkan waktu selesai nya acara
     * @param costPerHourStr adalah String yg menyimpan biaya acara perjam
     * @return String hasil kondisi saat pengecekan event
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {

        String[] startSplit = startTimeStr.split("_");
        String[] startDate = startSplit[0].split("-");
        String[] startTime = startSplit[1].split(":");
        GregorianCalendar start = new GregorianCalendar(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]) - 1,
                Integer.parseInt(startDate[2]), Integer.parseInt(startTime[0]), Integer.parseInt(startTime[1]),
                Integer.parseInt(startTime[2]));

        String[] endSplit = endTimeStr.split("_");
        String[] endDate = endSplit[0].split("-");
        String[] endTime = endSplit[1].split(":");
        GregorianCalendar end = new GregorianCalendar(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]) - 1,
                Integer.parseInt(startDate[2]), Integer.parseInt(startTime[0]), Integer.parseInt(startTime[1]),
                Integer.parseInt(startTime[2]));

        BigInteger costPerHour = new BigInteger(costPerHourStr);

        if (end.compareTo(start) < 0) return "Waktu yang diinputkan tidak valid!";
        else if (getEvent(name) != null) return "Event " + name + " sudah ada!";
        else {
            events.add(new Event(name));
            return "Event " + name + " berhasil ditambahkan!";
        }
    }

    /**
     * fungsi untuk menambah user baru jika user tersebut belum terdaftar
     *
     * @param name, name adalah String yang menyimpan nama user
     * @return String status user saat ingin ditambahkan
     */
    public String addUser(String name) {
        // TODO: Implement!
        for (User user : users) {
            if (name.equals(user.getName())) return "User " + name + " sudah ada!";
        }
        this.users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }

    /**
     * fungsi untuk menambah acara user sesuai dengan event yg dipilih
     *
     * @param userName adalah String yg menunjukkan nama user yg dipilih
     * @param eventName adalah String yg menunjukkan nama event yg ingin dipilih
     * @return String kondisi hasil pengecekan userName dan eventName
     */
    public String registerToEvent(String userName, String eventName) {
        // TODO: Implement
        if (getUser(userName) == null && getEvent(eventName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (getUser(userName) == null) return "Tidak ada pengguna dengan nama " + userName + "!";
        else if (getEvent(eventName) == null) return "Tidak ada acara dengan nama " + eventName + "!";
        else return "";
    }
}