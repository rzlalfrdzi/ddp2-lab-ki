package character;

//  write Monster Class here
public class Monster extends Player{

    private String Roar ;

    public Monster(String name, int hp) {
        super(name, hp);
        this.type = "Monster";
        this.hp = 2 * hp;
        setRoar("AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public Monster(String name,  int hp, String roar){
        super(name, hp*2, "Monster");
        setRoar(roar);
    }


    public String Roar(){

        return this.Roar;
    }

    @Override
    public void setRoar(String roar) {
        Roar = roar;
    }
}