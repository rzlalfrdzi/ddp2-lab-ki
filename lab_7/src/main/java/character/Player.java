package character;
import java.util.ArrayList;

//  write Player Class here
public class Player {
    String name;
    int hp;
    String type;
    boolean isDead;
    String Roar;
    boolean isBaked;
    ArrayList<Player> diet = new ArrayList<Player>();
    public int getHp;

    public Player(String name, int hp, String type) {
        this.name = name;
        this.hp = hp;
        this.type = type;
    }

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    //GETTER & SETTER
    public void setBaked(boolean baked) {
        isBaked = baked;
    }

    public boolean isBaked() {
        return isBaked;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public int getHp() {
        return hp;
    }

    public String getRoar() {
        return Roar;
    }

    public boolean isDead() {
        isDead = false;
        if ( this.hp ==0){
            isDead = true;
            return isDead;
        }
        else{
            return isDead;
        }
    }

    public void setRoar(String roar) {
        Roar = roar;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }


    //METHOD
    public String attack(Player enemy){
        if(enemy.getType().equals("Magician")){
            enemy.setHp(enemy.getHp()-20);
        }
        else {
            enemy.setHp(enemy.getHp()-10);
        }
        return "Nyawa" + enemy.getName() + enemy.getHp();
    }

    public String eat(Player enemy){
        if(isDead()){
            this.diet.add(enemy);
            this.hp += 15;
            return String.format("%s memakan %s nyawa %s kini %d",this.name , enemy.getName() , this.hp );
        }
        else {
            return String.format("%s tidak bisa memakan %s", this.name, enemy.getName());
        }
    }

    public void willbeEaten(Player enemy){
        this.diet.add(enemy);
    }

    public String diet(){
        String eaten = "" ;
        if (diet.size() != 0){
            for(int i = 0; i < diet.size(); i++){
               eaten += this.diet.get(i).getType() + this.diet.get(i).getName();
            }
            return "Memakan" + eaten;
        }
        return "Belum makan siapa siapa";
    }


}
