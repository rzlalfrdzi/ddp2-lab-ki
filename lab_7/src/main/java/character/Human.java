package character;

public class Human extends Player{
    public Human(String name, int hp){
        super(name, hp, "Human");
    }

    public boolean canEat(Player enemy){
        return enemy.getType().equals("Monster") && enemy.isBaked() && enemy.isDead();
    }

    public String eat(Player enemy){
        if(enemy.getType().equals("Monster")){
            return this.name + "Tidak bisa memakan" + enemy.getName();
        }
        else if( enemy.isDead() && enemy.isBaked()){
            this.diet.add(enemy);
            setHp(getHp() + 15);
            return this.name + "Memakan" + enemy.getName() + "Nyawa" + enemy.getName() + "kini" + this.hp;
        }
        else {
            return this.name + "Tidak bisa memakan" + enemy.getName();
        }
    }
}