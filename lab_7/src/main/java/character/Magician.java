package character;


//  write Magician Class here
public class Magician extends Player{
    public Magician(String name, int hp, String type){
        super(name, hp, type);
        this.type="Magician";
    }


    public String burn(Player enemy){
        String burn = "";
        if (enemy.isDead() == false && enemy.isBaked() == false){
            if(enemy.getType().equals("Magician")){
                enemy.setHp(enemy.getHp()-20);
                burn = "Nyawa" + enemy.getName() + " " + enemy.getHp();

                if(enemy.getHp == 0){
                    burn += "\ndan matang";
                    enemy.setBaked(true);
                }
            }
        }
        return burn;
    }
}