import character.*;
import java.util.ArrayList;
import java.util.List;

public class Game{
    ArrayList<Player> eatenPlayer = new ArrayList<>();
    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (int i = 0; i < player.size();i++){
            if (player.get(i).equals(name)){
                return player.get(i);
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player addPlayer = find(chara);
        if (addPlayer == null){
            if(tipe.equals("Monster")){
                Monster listMonster = new Monster(chara, hp, tipe);
                player.add(listMonster);
            }
            if (tipe.equals("Magician")){
                Magician listMagician = new Magician(chara, hp, tipe);
                player.add(listMagician);
                }
            if (tipe.equals("Human")){
                Human listHuman = new Human(chara, hp);
                player.add(listHuman);
            }
            return chara + " ditambah ke game";
        }
        else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */



    public String add(String chara, String tipe, int hp, String roar) {
        Player addPlayer = find(chara);
        if (addPlayer == null) {
            if (tipe.equals("Monster")) {
                Monster listMonster = new Monster(chara, hp, roar);
                player.add(listMonster);
            }
            if (tipe.equals("Magician")) {
                Magician listMagician = new Magician(chara, hp, tipe);
                player.add(listMagician);
            }
            if (tipe.equals("Human")) {
                Human listHuman = new Human(chara, hp);
                player.add(listHuman);
            }
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        for(int i = 0; i < player.size(); i++){
            if(player.get(i).equals(chara)){
                player.remove(i);
                return chara + " dihapus dari game";
            }
        }
        return "Tidak ada " + chara;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        Player findPlayer = find(chara);
        String result = "";
        if(findPlayer == null){
            result += "Tidak ada " + chara;
        }
        else{
            if(findPlayer.isDead() == true){
                result += findPlayer.getType() + " " + findPlayer.getName() + " \nHP : " + findPlayer.getHp() + "\nMasih hidup  " + "\n" + diet(chara);
            }
            else {
                result += findPlayer.getType() + " " + findPlayer.getName() + "\nHP: " + findPlayer.getHp() + "\nSudah meninggal dunia dengan damai\n" + diet(chara);
            }
        }
       return result;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String result = "";
        for (Player element : player){
            result += status(element.getName()) + "\n";
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        Player findDiet = this.find(chara);
        int totalDiet = findDiet.getDiet().size();
        if (findDiet == null) {
            return "Tidak ada karakter" + chara;
        } else {
            String eaten = "";
            ;
            if (totalDiet != 0) {
                for (int i = 0; i < totalDiet; i++) {
                    eaten += findDiet.getDiet().get(i).getType() + findDiet.getDiet().get(i).getName();
                    return eaten;
                }
            }
        }
        return null;
    }





    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String result = "Has been eaten: ";
        for(Player element : this.eatenPlayer){
            result += element.getType() + element.getName() + ",";
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player findAttacker = this.find(meName);
        Player findEnemy = this.find(enemyName);
        String result = "Nyawa ";
        findAttacker.attack(findEnemy);
        result += findEnemy.getName() + " "+ findEnemy.getHp();
        return result;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Magician findAttacker = (Magician) this.find(meName);
        Player findEnemy = this.find(enemyName);
        return findAttacker.burn(findEnemy);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player findEater = this.find(meName);
        Player findEaten = this.find(enemyName);
        if(findEaten.getType().toLowerCase().equals("monster") &&findEaten.isDead() && findEaten.isBaked() && ((findEater.getType().toLowerCase().equals("human")) || findEater.getType().toLowerCase().equals("magician"))){
            eatenPlayer.add(findEaten);
            player.remove(eatenPlayer);
            return findEater.eat(findEaten);
        }
        return null;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Monster findRoar = (Monster) this.find(meName);
        if (findRoar.getType().toLowerCase().equals("monster")){
            return findRoar.Roar();
        }
        return null;
    }
}