package customer;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;

public class Customer {
    String name;
    boolean gender; // female = true
    int age;

    public Customer(String name, String gender, int age){
        this.name = name;
        if(gender.toLowerCase().equals("laki-laki")){
            this.gender = true;
        }
        if(gender.toLowerCase().equals("perempuan")){
            this.gender = false;
        }
        this.age = age;
    }

    public Ticket orderTicket(Theater theater, String day, String title, String type){
        for(int i = 0; i < theater.getTicket().size(); i++){
            Ticket ticket = theater.getTicket().get(i);

        if(ticket.getMovie().getTitle().equals(title)){
            if(ticket.getDay().equals(day) && ((ticket.getType() == true && type.toLowerCase().equals("3 dimensi")) || (ticket.getType() == false && type.toLowerCase().equals("biasa")))){
                if((ticket.getMovie().getRating().toLowerCase().equals("remaja") && this.age < 13) || (ticket.getType() == false && type.toLowerCase().equals("biasa"))){
                    System.out.println(String.format("%s is not old enough to watch %s", this.name, title, ticket.getMovie().getRating()));
                    return null;
                }
                else {
                    theater.setCash(theater.getCash() + ticket.getPrice());
                    System.out.println(String.format("%s has purchased %s type %s at %s on %s for Rp. %s", this.name, title, type, theater.getName(), ticket.getDay(), theater.getCash()));
                    return ticket;
                }
            }
        }
    }
        System.out.println(String.format("Tickets for the movie %s type %s on %s are not available in %s",title, type, day, theater.getName()));
        return null;
  }

  public void findMovie(Theater theater, String title){
        for(int i = 0; i < theater.getTicket().size(); i++){
            Ticket ticket = theater.getTicket().get(i);
            if(ticket.getMovie().getTitle().equals(title)){
                if((ticket.getMovie().getRating().toLowerCase().equals("remaja") && this.age < 13) || (ticket.getMovie().getRating().toLowerCase().equals("dewasa") && this.age < 17)){
                    System.out.println(String.format(" %s is not old enough to watch %s",this.name, title, ticket.getMovie().getRating()));
                }
                else{
                    System.out.println(ticket.getMovie());
                }
                return;
            }
        }
      System.out.println(String.format("Movie %s searched by %s is not in the cinema %s",title, this.name, theater.getName()));
    }
}