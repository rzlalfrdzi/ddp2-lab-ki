package movie;

public class Movie {
    String title;
    String genre;
    int duration;
    String rating;
    String type;

    public String getRating(){
        return this.rating;
    }

    public String getTitle() {

        return this.title;
    }

    public String getGenre() {
        return this.genre;
    }

    public Movie(String title, String genre, int duration, String rating, String type){
        this.title = title;
        this.genre = genre;
        this.duration = duration;
        this.rating = rating;
        this.type = type;
    }

    public  void PrintMovie(){
        System.out.printf("--------------------------------------------\n" +
                            "Title:     %s\n"+
                            "Genre:     %s\n"+
                            "Duration:  %d Minutes \n"+
                            "Rating:    %s\n"+
                            "Type:      %s Film\n"+
                            "-------------------------------------------",
                            this.title, this.genre, this.duration, this.rating, this.type);
    }
}

