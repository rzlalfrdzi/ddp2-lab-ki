package ticket;
import movie.Movie;

public class Ticket {
    Movie movie;
    String day; //if Saturday or Sunday + 40000
    boolean type; // 2D or 3D * 1.2
    int price = 60000;

    public Ticket(Movie movie, String day, boolean type){


        this.movie = movie;
        this.day = day;
        this.type = type;

        if(day.toLowerCase().equals("sabtu") || day.toLowerCase().equals("minggu")){
            this.price += 40000;
        }
        if(this.type == true){
            price *= 1.2;
        }
    }

    public Movie getMovie() {
        return this.movie;
    }

    public String getDay() {
        return this.day;
    }

    public int getPrice() {
        return this.price;
    }

    public boolean getType() {
        return this.type;
    }

    public void PrintTicket(){
        System.out.printf("------------------------------------------\n" +
                            "Movies               :%s\n" +
                            "Showtimes            :%s\n"+
                            "Type                 :%s\n"+
                          "------------------------------------------",
                            this.movie, this.day, this.type);
    }
}

