package theater;
import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Theater {
    String name;
    int cash;
    ArrayList<Ticket> ticket;
    Movie[] movie;
    ArrayList<Theater> theater;

    public Theater(String name, int cash, ArrayList<Ticket> ticket, Movie[] movie){
        this.name = name;
        this.cash = cash;
        this.ticket = ticket;
        this.theater = theater;
        this.movie = movie;
    }

    //GETTER SETTER
    public String getName() {
        return this.name;
    }

    public int getCash() {
        return this.cash;
    }

    public ArrayList<Ticket> getTicket() {
        return this.ticket;
    }

    public void setCash(int newCash) {
        this.cash = newCash;
    }

    public static void printTotalRevenueEarned(Theater[] theaters) {
        String result = "";
        int totalcash = 0;
        for(int i = 0; i < theaters.length; i++){
            totalcash += theaters[i].getCash();
            if(i == 0){
                result  += String.format("Bioskop       : %s\n"+
                                         "Saldo Kas    : Rp. %d\n",
                                         theaters[i].getName(), theaters[i].getCash());
            }
            else {
                result  += String.format("Bioskop       : %s\n"+
                                         "Saldo Kas    : Rp. %d\n",
                                         theaters[i].getName(), theaters[i].getCash());
            }
        }
        System.out.println(String.format("Total uang yang di miliki koh mas : Rp. %d\n"+
                                         "-----------------------------------------------\n"+
                                          result + "\n" +
                                         "-----------------------------------------------",totalcash));

    }

    public void printInfo(){
        String movielist = "";
        for(int a = 0; a < this.movie.length; a++){
            if(a == 0){
                movielist += movie[a].getTitle();
            }
            else {
                movielist += ", " + movie[a].getTitle();
            }
        }
        System.out.println(String.format("-----------------------------------------\n"+
                                          "Cinema                       : %s\n"+
                                          "Cash Balance                 : %s\n"+
                                          "Number of tickets available  : %s\n"+
                                          "Movie list avaiable          : %s\n"+
                                          "----------------------------------------",
                                          this.name, this.cash, this.ticket.size(), movielist));
    }

}
