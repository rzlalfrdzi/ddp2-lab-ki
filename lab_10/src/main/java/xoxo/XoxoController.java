package xoxo;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.key.HugKey;
import xoxo.exceptions.*;
import xoxo.util.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;

/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(e -> decrypt());
        gui.setEncryptFunction(e -> encrypt());
    }

    /**
     * Encrypting the massage based on what the users input
     */
    public void decrypt() {
        String messageText = gui.getMessageText();
        String keyText = gui.getKeyText();
        String seedText = gui.getSeedText();
        int seed = HugKey.DEFAULT_SEED;
        if (!(seedText.equals("")) || !(seedText.contains("Insert Seed"))) {
            seed = Integer.parseInt(seedText);
        }
        XoxoDecryption decryption = null;
        try {
            decryption = new XoxoDecryption(keyText);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        if (messageText.equals("")) {
            JOptionPane.showMessageDialog(null,"There is no message to proccess.");
        } else {
            try{
                String decryptedMessage =  decryption.decrypt(messageText, seed);
                gui.appendLog("Decrypted message:\n"+
                        decryptedMessage +
                        "\n");
                writeDecrypt(decryptedMessage);
            } catch (RangeExceededException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            } catch (SizeTooBigException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }
    }

    /**
     * Encrypting the massage based on what the users input
     */
    public void encrypt() {
        String messageText = gui.getMessageText();
        String keyText = gui.getKeyText();
        String seedText = gui.getSeedText();
        int seed = HugKey.DEFAULT_SEED;
        if (!(seedText.equals("")) || !(seedText.contains("Insert Seed"))) {
            seed = Integer.parseInt(seedText);
        }
        XoxoEncryption encryption = null;
        try {
            encryption = new XoxoEncryption(keyText);
        } catch (KeyTooLongException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

        if (messageText.equals("")) {
            JOptionPane.showMessageDialog(null,"There is no message to proccess.");
        } else {
            try{
                XoxoMessage xoxoMessage=  encryption.encrypt(messageText, seed);
                HugKey hugKey = xoxoMessage.getHugKey();
                String encryptedMessage = xoxoMessage.getEncryptedMessage();
                gui.appendLog("Encrypted message :\n"+
                        encryptedMessage+ "\n" +
                        "Hug Key :\n" +
                        hugKey
                        );
                writeEncrypt(encryptedMessage);
            } catch (RangeExceededException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            } catch (SizeTooBigException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            } catch (InvalidCharacterException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }

        }
    }

    /**
     * Write the message to the new file named as "outputdecrypt.txt"
     *
     * @param message the message that will be written
     * @throws IOException
     */
    public void writeDecrypt(String message) throws IOException {
        File file = new File("outputdecrypt.txt");
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        writer.write(message);
        writer.flush();
        writer.close();
    }
    public void writeEncrypt(String message) throws IOException {
        File file = new File("outputencrypt.enc");
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        writer.write(message);
        writer.flush();
        writer.close();
    }
}