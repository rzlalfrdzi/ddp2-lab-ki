package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;


    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //initiate frame
        JFrame frame = new JFrame("Lab 10: XOXO Crypto");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setVisible(true);
        //initiate panel
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        // message text field
        messageField = new JTextField("Insert Message..");
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 300;
        c.ipadx = 100;
        panel.add(messageField, c);
        // KEY text field
        keyField = new JTextField("Insert Key..");
        c.gridx = 0;
        c.gridy = 1;
        c.ipady = 0;
        panel.add(keyField, c);
        // SEED text field
        seedField = new JTextField("Insert Seed..");
        c.gridx = 0;
        c.gridy = 2;
        c.ipady = 0;
        panel.add(seedField, c);
        // LOG text area
        logField = new JTextArea();
        c.gridx = 2;
        c.gridy = 0;
        c.ipadx = 100;
        panel.add(logField, c);
        // Encrypt Button
        encryptButton = new JButton("Encrypt");
        c.gridx = 2;
        c.gridy = 1;
        panel.add(encryptButton, c);
        // Decrypt Button
        decryptButton = new JButton("Decrypt");
        c.gridx = 2;
        c.gridy = 2;
        panel.add(decryptButton, c);
        // Adding panel to frame and pack it
        frame.add(panel);
        frame.pack();
    }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     *
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}